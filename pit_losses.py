import numpy as np
import pandas as pd
import os
import re
import argparse
import multiprocessing
import itertools
from enum import Enum

from scipy import stats
from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import ticker

import fastf1
import fastf1.plotting

fastf1.plotting.setup_mpl()
CACHE = "./cache"
if not os.path.exists(CACHE):
    os.mkdir(CACHE)

fastf1.Cache.enable_cache(CACHE)


class TrackStatus(Enum):
    AllClear = "1"
    Yellow = "2"
    SCDeployed = "4"
    Red = "5"
    VSCDeployed = "6"
    VSCEnding = "7"


def get_status(time, track_status) -> TrackStatus:
    """Produce track status at the given time"""

    first_above = np.argmax(track_status.Time.dt.seconds > time)

    if first_above == 0:
        return TrackStatus.AllClear.value

    return track_status.loc[first_above - 1].Status


def render_frame(
    dt, colors, times, mus, stds, driver_names, col, status_changes, prefix=None
):
    """Driver function for rendering one frame to be run in parallel"""

    if prefix is None:
        prefix = "pitgaps"

    # Render positions with pit delay overlay
    fig = pit_delay_fig(times[:, col], mus, stds, driver_names, colors)

    # Add title with time and track status
    ax = fig.get_axes()[0]
    time = col * dt
    time_msg = f"{int(time // 60):d}:{int(time % 60):02d}"
    status = get_status(time, status_changes)
    if status == TrackStatus.Yellow.value:
        ax.set_title(f"{time_msg}\nYellow Flag", color="y")
    elif status == TrackStatus.Red.value:
        ax.set_title(f"{time_msg}\nRed Flag", color="r")
    elif status == TrackStatus.SCDeployed.value:
        ax.set_title(f"{time_msg}\nSafety Car", color="y")
    elif status in [TrackStatus.VSCDeployed.value, TrackStatus.VSCEnding.value]:
        ax.set_title(f"{time_msg}\nVirtual Safety Car", color="y")
    else:
        ax.set_title(time_msg)

    fig.savefig(f"{prefix}_{col:06d}.png")
    plt.close(fig)


def pit_delay_fig(
    times: np.array,
    mus: np.array,
    stds: np.array,
    names: np.array,
    colors: np.array = None,
    xmax: float = 90,
):
    """Plot pit result predictions

    Args:
        times (np.array): Gap to leader for each driver
        mus (np.array): Mean overall pit time for each driver
        stds (np.array): Standard deviation of overall pit time for each driver
        names (np.array): Name of each driver
        colors (np.array, optional): Color to plot for each driver. Defaults to None.
        xmax (float, optional): Maximum gap to leader shown on plot axis. Defauts to 90.

    Returns:
        plt.Figure: Plotted standings with pit prediction overlay
    """

    sorted_i = np.argsort(times)
    times = np.array(times)[sorted_i]
    mus = np.array(mus)[sorted_i]
    names = np.array(names)[sorted_i]
    if colors is not None:
        colors = np.array(colors)[sorted_i]

    t = np.linspace(0, np.nanmax(times) + np.max(mus) + 3 * np.max(stds), 1000)

    fig, ax = plt.subplots(figsize=(16, 9), dpi=120)
    if colors is None:
        colors = mpl.cm.tab20(np.arange(20))

    arrow_y = 1.1
    arrow_dy = 0.1
    y_max = 4
    labels = [
        f"{name} " + (f"{time:+06.2f}" if not np.isnan(time) else "DNF")
        for name, time in zip(names, times)
    ]
    for time, mu, std, name, color, label in zip(
        times, mus, stds, names, colors, labels
    ):
        distr = stats.norm.pdf(t, mu + time, std)
        distr = distr / np.max(distr)

        ax.plot(t, distr, label=label, color=color)[0]
        ax.fill_between(t, distr, color=color, alpha=0.5)

        # Draw arrow over top
        ax.plot(time, 0.05, "^", alpha=0.5, color=color)
        ax.plot([time, time], [0, arrow_y], "--", alpha=0.5, color=color)
        ax.plot([time, time + mu], [arrow_y, arrow_y], "--", alpha=0.5, color=color)
        ax.plot([time + mu, time + mu], [arrow_y, 1], "--", alpha=0.5, color=color)
        ax.plot(time + mu, 1, "v", alpha=0.5, color=color)

        arrow_y += arrow_dy

    ax.set_ylim((0, y_max))
    ax.tick_params(labelleft=False)
    ax.tick_params(which="both", left=False)
    ax.grid(False, which="both")
    ax.xaxis.set_minor_locator(ticker.NullLocator())
    ax.set_xticks(times, labels=labels, rotation=-90)
    ax.set_xlim(right=xmax)
    for label, color in zip(plt.gca().get_xticklabels(), colors):
        label.set_color(color)

    ax.legend(loc=(1, 0))

    return fig


def find_dnf_times(session, start_time, drivers):
    """Identify DNF times"""
    dnf_times = []
    for driver_num in drivers:
        if pd.isnull(session.get_driver(driver_num).Time):
            car = session.car_data[driver_num]
            speed = car.Speed
            dnf_i = (
                (len(speed) - 1)
                - np.argmax(np.isclose(speed.diff(-1).iloc[:-1], 0)[::-1] == False)
                - 1
            )
            dnf_times.append(car.SessionTime.iloc[dnf_i] - start_time)

        else:
            dnf_times.append(None)

    return dnf_times


def main():
    parse = argparse.ArgumentParser(
        description="Produce series of images with driver positions and predicted losses due to a standard pit stop"
    )
    parse.add_argument("race", help="GP city")
    parse.add_argument("year", type=int, help="Race year")
    parse.add_argument(
        "mean_pit",
        type=float,
        help="Mean number of seconds lost for a standard pit stop at the given race",
    )
    parse.add_argument(
        "--std_pit",
        type=float,
        default=0.5,
        help="Standard deviation of seconds lost for a standard pit stop at the given race",
    )
    parse.add_argument(
        "--dt",
        type=float,
        default=5,
        help="Time interval in seconds between consecutive render frames",
    )
    parse.add_argument(
        "--threads",
        type=int,
        default=8,
        help="Number of concurrent threads to use for rendering frames",
    )
    args = parse.parse_args()

    # Open race dataset
    session = fastf1.get_session(args.year, args.race, "R")
    session.load(weather=False)

    start_time = session.session_start_time

    # Retrieve timing data for all drivers
    _, df = fastf1.api.timing_data(session.api_path)
    df = df.loc[df.Time >= start_time]

    # Initial times match start time from session status - Offset to zero at start
    track_status = pd.DataFrame(fastf1.api.track_status_data(session.api_path))
    track_status = track_status.loc[track_status.Time >= start_time].reset_index(
        drop=True
    )
    track_status.Time = track_status.Time - start_time

    # Basic driver attributes
    drivers = session.drivers
    driver_names = [session.get_driver(n).Abbreviation for n in drivers]
    colors = [f"#{session.get_driver(n).TeamColor}" for n in drivers]

    # Identify DNF times
    dnf_times = find_dnf_times(session, start_time, drivers)

    # Raw timing data is given at very irregular/inconsistent intervals
    # => Resample all drivers' times to consistent intervals
    time_df = session.results
    total_time = time_df.loc[time_df.Position == 1].Time.iloc[0].seconds
    n_steps = int(total_time / args.dt)
    x_resample = args.dt * np.arange(n_steps) + df.Time.dt.total_seconds().min()

    # Rough guess at time to add for being a lap down (because only laps-down are given for lapped drivers)
    lap_time = 1.1 * session.laps.pick_fastest().LapTime.seconds

    gaps_to_leader = []
    for driver_num, dnf_time in zip(drivers, dnf_times):
        driver_data = df.loc[df["Driver"] == driver_num].drop_duplicates("Time")
        driver_times = driver_data.GapToLeader

        # The leader's time is only given as the lap number
        driver_times = driver_times.replace(r"LAP \d+", 0, regex=True)

        # Replace any lapped entries with approximations
        driver_times = driver_times.replace(r"\d+ ?L", lap_time, regex=True)
        lapped_times = (
            driver_times.replace(r"(\d+) ?L", r"$1", regex=True).to_numpy(dtype=float)
            * lap_time
        )
        driver_times = driver_times.mask(
            [re.search(r"\d+ ?L", str(x)) is not None for x in driver_times],
            lapped_times,
        )

        # Replace missing entries
        driver_times = driver_times.fillna(0).to_numpy(dtype=float)

        # Resample
        x = np.array(driver_data.Time.dt.total_seconds())
        y = np.array(driver_times, dtype=float)
        y_resample = interp1d(x, y, "linear", fill_value="extrapolate")(x_resample)
        if dnf_time is not None:
            y_resample[
                x_resample > dnf_time.total_seconds() + start_time.total_seconds()
            ] = np.nan

        gaps_to_leader.append(y_resample)

    gaps_to_leader = np.array(gaps_to_leader, dtype=float)

    # Pit stop time loss normal distribution parameters
    mus = args.mean_pit * np.ones_like(gaps_to_leader[:, 0])
    stds = args.std_pit * np.ones_like(gaps_to_leader[:, 0])

    # Divide frames to render amongst threads
    pool = multiprocessing.Pool(args.threads)
    pool.starmap(
        render_frame,
        zip(
            itertools.repeat(args.dt),
            itertools.repeat(colors),
            itertools.repeat(gaps_to_leader),
            itertools.repeat(mus),
            itertools.repeat(stds),
            itertools.repeat(driver_names),
            range(gaps_to_leader.shape[1]),
            itertools.repeat(track_status),
        ),
    )


if __name__ == "__main__":
    main()
