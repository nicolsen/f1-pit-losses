# F1 Pit Losses

Visual plotting of F1 race positions with predictions for the effect of a standard pit stop overlaid.

_Author_: Nic Olsen

<div align="center"><img src="example.png" width="90%" alt="Monacco 2022 - 11:05"></div>


## Usage

- `pit_losses.py`
- Zip together resulting `png` files
    - e.g. `ffmpeg -r 20 -s 1920x1080 -f image2 -i gaps_%06d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p video.mp4`


## License

See [LICENSE](/LICENSE)